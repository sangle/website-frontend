import Head from "next/head";
import Link from "next/link";
import React from "react";

import Explanation from "../components/Explanation";
import H1 from "../components/H1";
import VerticallyCentered from "../components/VerticallyCentered";
import {
  I18nPage,
  includeDefaultNamespaces,
  Trans,
  useTranslation,
} from "../i18n";

const Page: I18nPage = () => {
  const { t } = useTranslation();
  return (
    <VerticallyCentered>
      <Head>
        <title>{t("_error:title")}</title>
      </Head>
      <H1 error={true}>{t("_error:h1")}</H1>
      <Explanation>
        <Trans i18nKey="_error:explanation">
          <a href="mailto:alexander@kachkaev.ru">email</a>
        </Trans>
        <br />
        <br />
        <Link href="/">
          <a>{t("signature")}</a>
        </Link>
      </Explanation>
    </VerticallyCentered>
  );
};

Page.getInitialProps = () => {
  return {
    namespacesRequired: includeDefaultNamespaces([]),
  };
};

export default Page;
