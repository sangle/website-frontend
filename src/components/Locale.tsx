import React, { FunctionComponent, useEffect, useState } from "react";
import { animated, useSpring } from "react-spring";
import styled from "styled-components";
import { format, UrlObject } from "url";

import { useTranslation } from "../i18n";

const CurrentLocale = styled.span`
  margin-left: 10px;
`;
const A = styled.a`
  margin-left: 10px;
  position: relative;
`;

const HighlightContainer = styled.span`
  position: absolute;
  left: 50%;
  top: 65%;
  width: 0;
  height: 0;
  pointer-events: none;
  z-index: -1;
`;

const HighlightRipple = styled(animated.div)`
  position: absolute;
  background: rgba(0, 0, 238);
  transition: background 0.2s ease-in-out;

  a:visited & {
    background: rgb(85, 26, 139);
  }
`;

const localeHighlightLocalStorageKey = "hideLocaleHighlightUntil";

const continueToHideHighlight = () => {
  try {
    localStorage.setItem(
      localeHighlightLocalStorageKey,
      `${new Date().getTime() + 7 * 24 * 60 * 60 * 1000}`,
    );
  } catch (e) {
    // noop
  }
};

const Highlight: FunctionComponent<{ children?: never }> = () => {
  const props = useSpring({
    from: {
      left: 0,
      top: 0,
      width: 0,
      height: 0,
      borderRadius: 0,
      opacity: 0,
    },
    config: { mass: 20, tension: 200, friction: 100, clamp: true },
    to: async (next) => {
      await next({
        left: -70,
        top: -70,
        width: 140,
        height: 140,
        borderRadius: 140,
        opacity: 0.2,
      });
      for (let i = 0; i < 3; i++) {
        await next({
          left: -11,
          top: -11,
          width: 22,
          height: 22,
          borderRadius: 22,
          opacity: 0.1,
        });
        await next({
          left: -17,
          top: -17,
          width: 34,
          height: 34,
          borderRadius: 34,
          opacity: 0.15,
        });
      }
      await next({
        left: 0,
        top: 0,
        width: 0,
        height: 0,
        borderRadius: 0,
        opacity: 0,
      });
    },
  });

  return (
    <HighlightContainer>
      <HighlightRipple style={props} />
    </HighlightContainer>
  );
};

const MemoisedHighlight = React.memo(Highlight);

const LinkToLocale: FunctionComponent<{
  targetLocale: string;
  href: string;
  children?: never;
}> = ({ targetLocale, href }) => {
  const [needToHighlight, setNeedToHighlight] = useState(false);
  const { i18n } = useTranslation();

  useEffect(() => {
    const supportedLanguages = navigator.languages || [];
    const currentLanguageIsSupported = supportedLanguages.find(
      (l) => l.substr(0, 2) === i18n.language,
    );
    if (!currentLanguageIsSupported) {
      const highlightHiddenUntil = parseInt(
        localStorage.getItem(localeHighlightLocalStorageKey),
        10,
      );
      if (highlightHiddenUntil > new Date().getTime()) {
        continueToHideHighlight();
      } else {
        setTimeout(() => {
          setNeedToHighlight(true);
        }, 1000);
      }
    }
  }, [i18n.language]);

  const handleClick = () => {
    continueToHideHighlight();
  };

  return (
    <A key={targetLocale} href={href} onClick={handleClick}>
      {targetLocale}
      {needToHighlight && <MemoisedHighlight />}
    </A>
  );
};

const Locale: FunctionComponent<{
  targetLocale: string;
  host: string;
  currentUrl: UrlObject;
  children?: never;
}> = ({ targetLocale, host, currentUrl }) => {
  const { i18n } = useTranslation();

  if (targetLocale === i18n.language) {
    return <CurrentLocale key={targetLocale}>{targetLocale}</CurrentLocale>;
  }

  return (
    <LinkToLocale
      targetLocale={targetLocale}
      href={format({ protocol: "https:", ...currentUrl, hostname: host })}
    />
  );
};

export default Locale;
