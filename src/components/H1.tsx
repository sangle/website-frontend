import styled from "styled-components";

const H1 = styled.h1<{ error?: boolean }>`
  font-size: 26px;
`;

export default H1;
